package br.senai.sp.informatica.empresat.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.empresat.dao.FuncionarioDao;
import br.senai.sp.informatica.empresat.model.Funcionario;

@WebServlet("/adicionaFuncionario")
public class AdicionaFuncionarioServlet extends HttpServlet {

	// sobrescreve o m�todo service
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// obt�m um objeto do tipo PrintWriter do response
		PrintWriter out = res.getWriter();
		
		// pega os par�metros do formul�rio de cadastramento dos funcion�rios (request)
		String nome = req.getParameter("nome");
		String email = req.getParameter("email");
		String senha = req.getParameter("senha");
		
		// instanciando funcion�rio
		Funcionario funcionario = new Funcionario();
		funcionario.setNome(nome);
		funcionario.setEmail(email);
		funcionario.setSenha(senha);
		
		// obt�m uma inst�ncia de contatodao e abre uma conex�o com o banco de dados
		FuncionarioDao dao = new FuncionarioDao();
		
		// salva o contato no banco de dados
		dao.salva(funcionario);
		
		// feedback para o funcion�rio
		out.println("<html>");
		out.println("<body>");
		out.println("Funcion�rio " + funcionario.getNome() + " salvo com sucesso!");
		out.println("</body");
		out.println("</html>");	
	}
}
