package br.senai.sp.informatica.empresat.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

	public Connection getConnection() {
		try {
			// registra o driver JDBC
			Class.forName("com.mysql.jdbc.Driver");

			// retorna uma conex�o com o banco de dados gerada pelo DriverManager
			return DriverManager.getConnection("jdbc:mysql://localhost/empresat", "root", "root132");
		} catch (SQLException e) {
			throw new RuntimeException(e); // throw passa o erro para a classe que o chamar
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
}
