package br.senai.sp.informatica.empresat.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.senai.sp.informatica.empresat.model.Funcionario;

public class FuncionarioDao {

	// atributo
	private Connection connection;
	
	// construtor
	public FuncionarioDao() {
		// estabelece uma conex�o com o banco de dados
		connection = new ConnectionFactory().getConnection();
	}
	
	// salva
	public void salva(Funcionario funcionario) {
		// cria um comando sql
		String sql = "INSERT INTO contato " + "(nome, email, senha) " + "VALUES (?, ?, ?)";
		
		try {
			// cria o executador do comando sql criado anteriormente, que no caso se chama PreparedStatement
			// o PreparedStatement � o cara que sabe como executar as fun��es sql, ou seja, os comandos l� do banco de dados (JDBC)
			// � ele que pega as strings e p�e no banco de dados e trata os poss�veis erros de aplica��o
			PreparedStatement stmt = connection.prepareStatement(sql);
			
			// cria os par�metros do PreparedStatement
			stmt.setString(1, funcionario.getNome());
			stmt.setString(2, funcionario.getEmail());
			stmt.setString(3, funcionario.getSenha());
			
			// executa o insert
			stmt.execute();
			
			// encerra e libera o recurso statement
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}	
	}
	
	// getLista
	public List<Funcionario> getLista() {
		
		try {
		// cria um ArrayList
		List<Funcionario> funcionarios = new ArrayList<>();
		
		// cria um PreparedStatement
		PreparedStatement stmt = connection.prepareStatement("SELECT * FROM contato");
		
		// executa o statement e guarda em um resultSet
		ResultSet rs = stmt.executeQuery();
		
		// enquanto houver dados no resultSet
		while (rs.next()) {
			// cria um contato com os dados do resultSet
			Funcionario funcionario = new Funcionario();
			funcionario.setId(rs.getLong("id"));
			funcionario.setNome(rs.getString("nome"));
			funcionario.setEmail(rs.getString("email"));
			funcionario.setSenha(rs.getString("senha"));
			
			// adiciona o funcion�rio � lista de funcion�rios
			funcionarios.add(funcionario);	
		}
		// fecha o resultSet
		rs.close();
		// fecha o statement
		stmt.close();
		// retorna a lista de funcion�rio
		return funcionarios;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}	
}
